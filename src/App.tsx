import React from 'react';
import './App.css';
import { WeatherDate } from './components/weather-date';
import { WeatherDisplay } from './components/weather-display';

const App: React.FC = () => {

  return (
    <div className="weather-container">
      <WeatherDate></WeatherDate>
      <WeatherDisplay></WeatherDisplay>
    </div>
  );
}

export default App;
