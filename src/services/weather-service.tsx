import { useEffect, useState } from 'react';
import { Service } from '../types/Service';
import { Weather } from '../types/Weather';

// I didn't end up using this service but, with more time I would get the 'weather-display' component call this instead

const useWeatherServiceByUrl = (url: string) => {
    const [result, setResult] = useState<Service<Weather>>({
      status: 'loading'
    });
  
    useEffect(() => {
      if (url) {
        setResult({ status: 'loading' });
        fetch(url)
          .then(response => response.json())
          .then(response => setResult({
            status: 'loaded', payload: {
                main: response.weather[0].main,
                city: response.name,
                description: response.weather[0].description,
                icon: response.weather[0].icon,
                temp: Math.round(response.main.temp)
            }
        }))
          .catch(error => setResult({ status: 'error', error }));
      }
    }, [url]);
  
    return result;
  };
  
  export default useWeatherServiceByUrl;