export interface Weather {
    temp: number,
    city: string;
    main: string,
    description: string,
    icon: string
}