import React from 'react';
import { Weather } from '../types/Weather';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export interface WeatherProps { longitude: string; latitude: string; }

export class WeatherDisplay extends React.Component {

    appId: string = '6dc9c2a99870e8a1a5ff5c72ecc15d4e';
    baseUrl = 'http://api.openweathermap.org/data/2.5/weather?';
    lat = '';
    lon = '';
    url = '';
    progressBarWidth = 100;

    thisWeather: Weather = {
        temp: 0,
        city: '',
        main: '',
        description: '',
        icon: ''
    };

    state = {
        loading: true,
        error: false
    };

    getDetails = () => {
        fetch(this.url)
            .then(response => response.json())
            .then(response => {
                this.thisWeather = {
                    main: response.weather[0].main,
                    city: response.name,
                    description: response.weather[0].description,
                    icon: response.weather[0].icon,
                    temp: Math.round(response.main.temp)
                }
                this.setState({ loading: false })
            })
            .catch(error => this.setState({
                loading: false,
                error: true
            }));
    }
    
    frame = () => {
        const elem = document.getElementById('progress')!;
        if (this.progressBarWidth === 0) {
            clearInterval();
            this.progressBarWidth = 100;
            this.getDetails();
        } else {
            this.progressBarWidth--;
            if(elem) {
                elem.style.width = this.progressBarWidth + '%';
            }
        }
    }

    componentDidMount() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.lon = '' + position.coords.longitude;
                this.lat = '' + position.coords.latitude;

                this.url = this.baseUrl + `lat=${this.lat}&lon=${this.lon}&units=metric&appid=${this.appId}`;

                this.getDetails();

                setInterval( () => {
                    this.frame();
                }, 300);

            }, () => {
                console.error('could not retrieve location');
                this.setState({ isLoaded: true });
            });
        }
    }



    render() {
        const { loading, error } = this.state;

        const iconUrl = "http://openweathermap.org/img/w/" + this.thisWeather.icon + ".png";

        return (
            <div>
                {loading &&
                    <div className="loader-container">
                        <div className="loader"></div>
                    </div>
                }
                {!loading && !error &&
                    <div className="weather">
                        <span id="progress" className="progressBar" style={{ width:this.progressBarWidth + '%'}}></span>
                        <div className="weather__icon">
                            <img id="wicon" src={iconUrl} alt="Weather icon" />
                            <div className="type">{this.thisWeather.main}</div>
                        </div>

                        <div className="weather__temp">
                            <span className="temp-value">{this.thisWeather.temp}</span>
                            <span className="temp-type"><sup>o</sup>C</span>
                        </div>
                        <div className="weather__city"><FontAwesomeIcon icon={faLocationArrow} />&nbsp;{this.thisWeather.city}</div>
                    </div>
                }
                {error && <div>Error message</div>}
            </div>
        )
    }
}