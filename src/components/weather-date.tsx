import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar } from '@fortawesome/free-solid-svg-icons'

export const WeatherDate = () => {

    const today = new Date();

    return <div className="weather-date"><FontAwesomeIcon icon={faCalendar} />&nbsp;&nbsp;{ today.toDateString() }</div>
}